# Movie App

Listado de tus cuatro peliculas favoritas.

## El reto

Utilizando Rails 6.1, HAML, TailwindCSS y ActiveRecord debes transformar la maqueta resultante del reto **Movie Markup** a una aplicación web.

Un framework nuevo puede ser intimidante, lo entendemos... es por esto que este reto es diferente a los anteriores.

Para este reto tienes tres niveles, puedes entregar el reto solo con el nivel uno; pero... nos gustaría saber cual fue tu estrategia para el siguiente nivel, que intentaste y porque consideras que no pudiste terminarlo (lo importante aqui es conocernos, ver como asumes retos y nuevas tecnologías) para que puedas escribir tus conclusiones tienes un apartado en este mismo archivo en el que podras escribir todo lo quieras que leamos.

### Nivel 1

Debes transformar el codigo de HTML a HAML y el SCSS a TailwindCSS, te recomendamos que lo hagas tu (hay muchas herramientas online que lo podrian hacer por ti), en aurorajobs nuestras vistas estan en HAML y nuestros estilos usan TailwindCSS, el hecho de que estes familiarizad@ te va a ayudar a leer más facilmente nuestro codigo.

#### Tips

- En este proyecto HAML y TailwindCSS estan instalados y deberian funcionar "out-of-the-box"
- En aurorajobs usamos TailwindCSS siguiendo el standard BEM, es por esto que no agregamos las clases de Tailwind directamente en el markup (HAML) lo hacemos mediante la directiva `@apply` podras ver un ejemplo al inicializar este repositorio
- Los archivos mas relevantes para este nivel son: `app/views/home/index.html.haml` y `app/javascript/css/style.css`

### Nivel 2

El listado de peliculas que se muestra en el home lo debes obtener desde la base de datos mediante el modelo Movie. Tienes la libertad de agregar los atributos que consideres necesarios.

En este nivel no es necesario que las imagenes se muestren desde la BD, puedes crear un campo llamado "image_link", guardar una imagen externa y luego mostrarla en la vista.

#### Tips

- No compliques el modelo de datos ni los atributos (creemos que un modelo y algunos campos seran mas que suficientes: name, position, image_link)
- Simplifica tu codigo al maximo (sin sacrificar elementos del diseño original)
- Una vez crees el modelo Movie, cargues al menos 3 peliculas utilizando seeds o la consola de rails.
- Al mostrar las peliculas desde la BD considera "pintar" la primera pelicula fuera del ciclo y luego en un ciclo pintes el resto (probablemente este tip no tenga sentido hasta que no empieces a desarrollar este nivel)

### Nivel 3

Debes usar active_storage para almacenar las imagenes del modelo Movie y crear seeds con al menos 5 peliculas pre-cargadas.

#### Tips

- No es necesario generar ninguna interfaz para cargar peliculas (se pueden pre-cargar mediante seeds o directamente en la consola)

---

Si tienes alguna duda sobre este reto puedes contactarnos en dev@aurorajobs.io

1. Debes hacer un fork de este repositorio (si no entiendes que es esto, no pasa nada... saber como funciona git es parte del reto)
2. Programar tu solución
3. Entregar el link del repositorio usando el sistema de careers de aurorajobs.

## Como usar este repositorio

En el template tienes instaladas las librerias y tienes una estructura basica donde empezar a codificar el reto. Lo importante de este reto no es crear la aplicación y configurar las librerias sino cumplir los requerimientos del reto.

1. Debes instalar ruby y rails en tu PC/Mac https://gorails.com/setup/ubuntu/22.04
2. Desde la terminal muevete a la raiz de este repositorio y ejecuta:
  - `bundle install`
  - `yarn install`
  - `rails db:create`
  - `./bin/webpack-dev-server`
3. En una nueva terminal muevete a la raiz de este repositorio y ejecuta `rails s`
4. Navega a http://localhost:3000 y deberías ver la vista inicial.

## El reto es muy dificil?

Haz tu mejor esfuerzo, si consideramos que nos gustaría saber más de tus decisiones tecnicas o tu idea esta plasmada en parte de tu trabajo pero aun no esta completa, posiblemente te contantemos para saber más de ti :)

## Notas del desarrollador

Realmente disfrute trabajando en estas pruebas tecnicas. Aprendí mucho, y pude saciar un poco mi hambre de conocimiento y estudio. Soy conciente, que quizas me demore más de lo que me habria gustado, pero a veces por las circunstancias no sale como se prevee.
Ruby, es un lenguaje que me llamaba mucho la atencion, y agradezco el haber tenido la posibilidad de hacer esta prueba tecnica, no solo por la oportunidad laboral, sino porque a lo largo de varios dias, entre asuntos personales, los workshop de cada semana, y las tareas del hogar, logre poder interiorizar conceptos y las bases del idioma. Más alla de si soy o no seleccionado, se me abrio una puerta, y tengo pensado seguir investigando y estudiando Ruby on Rails.
  
Con el 4EnLinea, lo pasa muy bien, y me diverti, peque de tomarme mucho tiempo en el, creo que no fui conciente de que iban a ver más instancias en la prueba tecnica. Luego de entragar el link del repositorio, me emocione al ver que la prueba seguia.

La maquetación basandome en el prototipo de figma, trate de hacerla lo más fiel posible basandome en los valores prestados en el mismo. 

La utilización tanto de HAML como de TailwindCSS no me fue un problema, Tailwind me parece muy potente a la hora de trabajar el estilo. 
HAML aunque en un princpio pense "¿Que es esto?", fue increible lo rapido que incorpore su mecanica y sintaxis, muy intuitivo. Las pocas dudas que tuve, me fueron respondidas en la documentación (como suele pasar siempre, la respuesta esta en la documentación ).

Por ultimo, y no menos importante, Rails!
Un framework con mucho alcance a disposición del desarrollador. Para interiorizarme, inicie un pequeño proyecto CRUD paralelamente, que me ayudo a comprender mejor el marco de trabajo y lograr el objetivo de esta prueba.

En este punto termine de convencerme de que Matz cumplio con su objetivc, porque a pesar de mi desconocimiento, fue una experiencia que me dio diversión, y que minimizo las sorpresas.

Nuevamente, gracias por esta oportunidad!
class CreateMovies < ActiveRecord::Migration[6.1]
  def change
    create_table :movies do |t|
      t.string :title
      t.string :author
      t.integer :position
      t.string :image_link

      t.timestamps
    end
  end
end

# Seeds para cargar base de datos con imagenes externos

## Array de hashes con información de peliculas con enlace externos a los poster
=begin
movies = [
    {:title=>'Enter The Void', 
        :author=>'Gaspar Noe', 
        :position=>1, 
        :image_link=>'https://image.tmdb.org/t/p/original/krKnsfvSJM1PL40tLicRhVQ6kuG.jpg'}, 
    
    {:title=>'Wrong', 
        :author=>'Quentin Dupieux', 
        :position=>2, 
        :image_link=>'https://thefilmstage.com/wp-content/uploads/2013/01/wrong_poster_1.jpg'},
    
    {:title=>'Climax', 
        :author=>'Gaspar Noe', 
        :position=>3, 
        :image_link=>'https://elpalomitron.com/wp-content/uploads/2018/09/MV5BMjllYmQ2OGQtN2IxZC00ODJiLWI4NjQtYmNlZjYzNzUzYjkyXkEyXkFqcGdeQXVyNTAzMTY4MDA@._V1_.jpg'},
    
    {:title=>'Réalité', 
        :author=>'Quentin Dupieux', 
        :position=>4, 
        :image_link=>'https://www.benzinemag.net/wp-content/uploads/2015/02/realite-affiche.jpg'}
]

### Agrega los elemetos a la base de datos
films = Movie.create(movies) 

=end

#Seeds para cargar base de datos, con imaganes a traves de active_storage y array de hashes


## Array de hashes con la información de las peliculas con rutas locales de los posters de las peliculas
films = [
    {:title=>'La Montaña Sagrada', 
        :author=>'Alejandro Jodorowsky', 
        :position=>5, 
        :image=>'app/assets/images/laMontañaSagradaAlejandroJodorowsky.jpg'},
        
    {:title=>'El Topo', 
        :author=>'Alejandro Jodorowsky', 
        :position=>6, 
        :image=>'app/assets/images/elTopoAlejandroJodorowsky.jpg'}, 
    
    {:title=>'Relatos Salvajes', 
        :author=>'Damian Szifron', 
        :position=>7, 
        :image=>'app/assets/images/relatosSalvajesDamianSzifron.jpg'},
    
    {:title=>'Un Cuento Chino', 
        :author=>'Sebastian Borensztein', 
        :position=>8, 
        :image=>'app/assets/images/unCuentoChinoSebastianBorensztein.jpg'},

    {:title=>'Irreversible', 
        :author=>'Gaspar Noe', 
        :position=>9, 
        :image=>'app/assets/images/irreversibleGasparNoe.png'}
]


### Metodo implementado para recorrer el array 'films' para agregar elementos a la base de datos, en cada instancia toma los elementos del hash y los utiliza
films.each do |film|
    movie = Movie.create(title: film[:title], author: film[:author], position: film[:position])
    ### Tambien, en cada instancia, agrega a traves de active_storage la imagen guardada localmente
    movie.image.attach(io: File.open(film[:image]), filename: "#{film[:title]}.#{film[:image].split('.')[-1]}", content_type: "image/#{film[:image].split('.')[-1]}")
end
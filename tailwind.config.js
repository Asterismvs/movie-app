/** @type {import('tailwindcss').Config} */
module.exports = {
  mode: 'jit',
  important: true,
  purge: {
    enabled: ["production", "staging"].includes(process.env.NODE_ENV),
    content: [
      './app/views/**/*.html.erb',
      './app/views/**/*.html.haml',
      './app/helpers/**/*.rb',
      './app/javascript/**/*.js',
    ],
  },
  theme: {
    container: {
      padding: {
        DEFAULT: '1rem',
        sm: '2rem',
        lg: '4rem',
        xl: '5rem',
        '2xl': '6rem',
      },
    },
    extend: {
      width: {
        body: '25.68rem',
        imageList: '8.75rem',
        
        },
      height: {
        body: '45.68rem',
        sectionList: '18.875rem', 
        filmList: '11.25rem',
        imageList: '11.25rem',
      },
      minWidth: {
        imageList: '8.75rem',
      },
      fontFamily: {
        montserrat: ["Montserrat", "sans-serif"],
      },
      backgroundImage:{
        linearGrad: 'linear-gradient(180deg, rgba(0, 0, 0, 0.20) 0%, rgba(0, 0, 0, 0.40) 5%, rgb(0, 0, 0.9) 40%)'
      }
    },
  }
}

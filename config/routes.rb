 Rails.application.routes.draw do
  # all your other routes
  #match '*unmatched', to: 'application#route_not_found', via: :all
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  root 'home#index'
end

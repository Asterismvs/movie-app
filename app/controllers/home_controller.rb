class HomeController < ApplicationController
  def index
    #Variable de instancia que contiene todos los elementos de la tabla
    @movies = Movie.all
    #Variable de instancia que contiene el primer elemento de la tabla
    @poster = Movie.first
    
  end
end
class Movie < ApplicationRecord
    has_one_attached :image
    validates :title, presence: true
    validates :author, presence: true
    validates :position, presence: true
end
